
class Assets {
  Assets._();

  static final fonts = _AssetsFonts._();
  static final icons = _AssetsIcons._();
  static final lang = _AssetsLang._();

}

class _AssetsFonts {
  _AssetsFonts._();


  final jfFlatBoldTTF = 'assets/fonts/JF_Flat_bold.ttf';
  final jfFlatRegularTTF = 'assets/fonts/JF_Flat_regular.ttf';
}

class _AssetsIcons {
  _AssetsIcons._();


  final articlePNG = 'assets/icons/article.png';
  final homePNG = 'assets/icons/home.png';
  final offerPNG = 'assets/icons/offer.png';
  final phonePNG = 'assets/icons/phone.png';
}

class _AssetsLang {
  _AssetsLang._();


  final arJSON = 'assets/lang/ar.json';
  final enJSON = 'assets/lang/en.json';
}
