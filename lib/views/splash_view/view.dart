import 'dart:async';

import 'package:flutter/material.dart';

import '../../main.dart';
import '../auth/sign_in/view.dart';
import '../nav_bar/view.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  bool flag = false;

  void goToPage() async {
    if (Prefs.getString('token') != null) {
      print("__________________________________________________ token");
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => const NavigationView(),
        ),
        (Route<dynamic> route) => false,
      );
      //is_verified
    } else if (Prefs.getString('isVerified') != '0' &&
        Prefs.getString('token') == null) {
      print("__________________________________________________ NO isVerified");
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) => SignInView(),
        ),
        (Route<dynamic> route) => false,
      );
    }
  }

  @override
  void initState() {
    // _chuckData();
    // Timer(4.seconds, () => _checkUser());
    Timer(
      const Duration(seconds: 2),
      () {
        goToPage();
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text("SplashView"),
      ),
    );
  }
}
