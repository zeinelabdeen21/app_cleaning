import 'package:app_cleaning/controller/login_bloc/state.dart';
import 'package:app_cleaning/views/home/home.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_cleaning/controller/login_bloc/bloc.dart';
import 'package:app_cleaning/controller/login_bloc/event.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kiwi/kiwi.dart';

import '../../../gen/locale_keys.g.dart';
import '../../../helpers/btn.dart';
import '../../../helpers/custom_text.dart';
import '../../../helpers/text_form.dart';

class SignInView extends StatefulWidget {
  const SignInView({super.key});

  @override
  State<SignInView> createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  final LoginEventStart _event = LoginEventStart();
  final LoginBloc _bloc = KiwiContainer().resolve<LoginBloc>();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool isPass = true;
    return Scaffold(
      body: Form(
        key: _event.formKey,
        child: Padding(
          padding: const EdgeInsets.only(
            right: 15,
            left: 15,
          ),
          child: ListView(
            children: [
              SizedBox(height: 100.h),
              Center(
                child: Container(
                  alignment: Alignment.center,
                  width: 140.w,
                  height: 140.h,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(100),
                  ),
                ),
              ),
              SizedBox(height: 100.h),
              CustomTextField(
                controller: _event.phone,
                validator: (v) {
                  if (v!.isEmpty) {
                    return LocaleKeys.Auth_wrong_mobile_number.tr();
                  } else {
                    return null;
                  }
                },
                onSaved: (o) {},
                hintText: tr(LocaleKeys.Auth_phone),
                enabled: true,
                obscureText: false,
                textInputType: TextInputType.number,
              ),
              SizedBox(height: 22.h),
              CustomTextFormBass(
                controller: _event.password,
                validator: (v) {
                  if (v!.isEmpty) {
                    return LocaleKeys.Auth_wrong_password.tr();
                  } else {
                    return null;
                  }
                },
                suffix: InkWell(
                  onTap: () {
                    setState(() {
                      isPass = !isPass;
                    });
                  },
                  child: const Icon(
                    Icons.visibility,
                    color: Color.fromARGB(255, 198, 190, 190),
                    size: 15,
                  ),
                ),
                onSaved: (o) {},
                hintText: LocaleKeys.Auth_password.tr(),
                enabled: true,
                obscureText: isPass,
                textInputType: TextInputType.visiblePassword,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 15,
                  bottom: 15,
                  right: 15,
                  left: 15,
                ),
                child: InkWell(
                  onTap: () {},
                  child: Text(tr(LocaleKeys.Auth_forgot_your_password)),
                ),
              ),
              BlocConsumer(
                bloc: _bloc,
                listener: (context, state) {
                  if (state is LoginStateSuccess) {
                    print("🐶🐶🐶🐶🐶🐶🐶🐶🐶🐶🐶🐶 ${state.model.message}");
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const HomeView(),
                      ),
                      (Route<dynamic> route) => false,
                    );

                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(state.model.message),
                    ));
                  } else if (state is LoginStateFailed) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(state.msg),
                    ));
                    print(
                        "🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑🌑 ${state.msg.toString()}");
                  }
                },
                builder: (context, snapshot) {
                  if (snapshot is LoginStatesStart) {
                    return const SpinKitHourGlass(
                      color: Colors.amber,
                      size: 40.0,
                    );
                  } else {
                    return Btn(
                      txt: tr(LocaleKeys.Auth_login),
                      onTap: () {
                        if (_event.formKey.currentState!.validate()) {
                          _event.formKey.currentState!.save();
                          _bloc.add(_event);
                        }
                      },
                    );
                  }
                },
              ),
              AuthTextAccount(
                text: tr(LocaleKeys.Auth_Dont_hav_account),
                title: tr(LocaleKeys.Auth_Create_an_account),
                onTap: () {},
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 10.0, right: 15.0),
                        child: const Divider(
                          color: Colors.black,
                          height: 50,
                        )),
                  ),
                  const Text("أو"),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.only(left: 15.0, right: 10.0),
                        child: const Divider(
                          color: Colors.black,
                          height: 50,
                        )),
                  ),
                ],
              ),
              const Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(12.0),
                    child: Text(
                      'الدخول كزائر',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
