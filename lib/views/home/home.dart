import 'package:app_cleaning/controller/home_bloc/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_cleaning/controller/home_bloc/event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiwi/kiwi.dart';

import '../../controller/home_bloc/bloc.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    HomeBloc _bloc = KiwiContainer().resolve()..add(HomeEventStart());
    return BlocBuilder(
      bloc: _bloc,
      builder: (context, state) {
        if (state is HomeStateStart) {
          return Center(
            child: Container(
              width: 30,
              height: 30,
              child: const CircularProgressIndicator(),
            ),
          );
        } else if (state is HomeStateSuccess) {
          return ListView.builder(
            itemCount: state.model.data.length,
            itemBuilder: (context, i) {
              return Padding(
                padding: const EdgeInsets.only(
                    left: 15, right: 15, bottom: 2, top: 2),
                child: ExpansionTile(
                    backgroundColor: Color.fromARGB(83, 248, 248, 248),
                    collapsedBackgroundColor: Color.fromARGB(22, 186, 21, 21),
                    collapsedIconColor: const Color.fromARGB(255, 134, 97, 97),
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    collapsedShape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    title: Container(
                      height: 50,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(),
                      // color: Color(0xFFBA1615),
                      child: Row(
                        children: [
                          Image.network(
                            state.model.data[i].image,
                            width: 50.w,
                            fit: BoxFit.contain,
                          ),
                          SizedBox(
                            width: 20.w,
                          ),
                          Text(
                            state.model.data[i].titleAr,
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                    ),
                    children: [
                      if (state.model.data[i].subsection.isEmpty)
                        Padding(
                          padding: const EdgeInsets.only(
                              right: 30, left: 30, bottom: 10),
                          child: Container(
                            height: 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: const Color(0xFFFFF2F2),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: const Text("لا توجد أقسام"),
                          ),
                        ),
                      ...List.generate(
                        state.model.data[i].subsection.length,
                        (index) => Padding(
                          padding: const EdgeInsets.only(
                              right: 30, left: 30, bottom: 10),
                          child: Container(
                            height: 50,
                            decoration: BoxDecoration(
                              color: const Color(0xFFFFF2F2),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              state.model.data[i].subsection[index].titleAr,
                              style: const TextStyle(
                                  color: Color(0xFF394739), fontSize: 15),
                            ),
                          ),
                        ),
                      ),
                    ]),
              );
            },
          );
        } else if (state is HomeStateFailed) {
          return Center(
            child: Text(state.msg),
          );
        } else {
          return Center(
            child: Container(
              width: 30,
              height: 30,
              child: const CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
