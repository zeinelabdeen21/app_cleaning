import 'package:app_cleaning/controller/home_bloc/event.dart';
import 'package:app_cleaning/controller/home_bloc/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/home_model.dart';
import '../../services/server_gate.dart';
import '../../services/user_token.dart';

class HomeBloc extends Bloc<HomeEvents, HomeStates> {
  HomeBloc() : super(HomeStates()) {
    on<HomeEvents>((event, emit) async {
      await mapEventToState(event, emit);
    });
  }
  ServerGate serverGate = ServerGate();

  Future mapEventToState(HomeEvents event, Emitter<HomeStates> emit) async {
    if (event is HomeEventStart) {
      emit(HomeStateStart());
      Map<String, dynamic> headers = await headersMap();
      CustomResponse response = await serverGate.getFromServer(
        // headers: headers,
        url: 'sections/index',
      );
      if (response.success) {
        HomeModel model = HomeModel.fromJson(response.response!.data);
        emit(HomeStateSuccess(model: model));
      } else {
        emit(HomeStateFailed(errType: response.errType!, msg: response.msg));
      }
    }
  }
}
