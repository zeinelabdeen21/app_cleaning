import 'package:app_cleaning/main.dart';
import 'package:app_cleaning/controller/login_bloc/event.dart';
import 'package:app_cleaning/models/model.dart';
import 'package:app_cleaning/controller/login_bloc/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../services/server_gate.dart';
import '../../services/user_token.dart';

class LoginBloc extends Bloc<LoginEvents, LoginStates> {
  LoginBloc() : super(LoginStates()) {
    on<LoginEvents>((event, emit) async {
      await mapEventToState(event, emit);
    });
  }
  ServerGate serverGate = ServerGate();

  Future mapEventToState(LoginEvents event, Emitter<LoginStates> emit) async {
    if (event is LoginEventStart) {
      emit(LoginStatesStart());
      Map<String, dynamic> headers = await headersMapWithoutToken();
      CustomResponse response = await serverGate.sendToServer(
        url: 'user/login',
        headers: headers,
        body: {
          'phone': event.phone.text,
          'password': event.password.text,
        },
      );
      if (response.success) {
        LoginModel model = LoginModel.fromJson(response.response!.data);
        Prefs.setInt('id', model.data.id ?? 0);
        Prefs.setString('name', model.data.name ?? "");
        Prefs.setString('email', model.data.email ?? "");
        Prefs.setString('phone', model.data.phone ?? "");
        Prefs.setString('image', model.data.image ?? "");
        Prefs.setString('fcm_token', model.data.fcmToken ?? "");
        Prefs.setString('isVerified', model.data.isVerified ?? "");
        Prefs.setString('token', model.data.token);
        emit(LoginStateSuccess(model: model));
      } else {
        emit(LoginStateFailed(
          errType: response.errType!,
          msg: response.msg,
        ));
      }
    }
  }

  // void _sendData(
  //   LoginEventStart event,
  //   Emitter<LoginStates> emit,
  // ) async {
  //   emit(LoginStatesStart());
  //   CustomResponse response = await serverGate.sendToServer(
  //     url: 'user/login',
  //     body: {
  //       'phone': event.phone.text,
  //       'password': event.password.text,
  //     },
  //   );
  //   if (response.success) {
  //     LoginModel model = LoginModel.fromJson(response.response!.data);
  //     Prefs.setInt('id', model.data.id);
  //     Prefs.setString('name', model.data.name);
  //     Prefs.setString('email', model.data.email);
  //     Prefs.setString('phone', model.data.phone);
  //     Prefs.setString('image', model.data.image);
  //     Prefs.setString('fcm_token', model.data.fcmToken);
  //     Prefs.setString('status', model.data.status);
  //     Prefs.setString('isVerified', model.data.isVerified);
  //     Prefs.setString('token', model.data.token);
  //     emit(LoginStateSuccess(model: model));
  //   } else {
  //     LoginStateFailed(
  //       errType: response.errType!,
  //       msg: response.msg,
  //     );
  //   }
  // }
}
