import 'package:app_cleaning/models/model.dart';

class LoginStates {}

class LoginStatesStart extends LoginStates {}

class LoginStateSuccess extends LoginStates {
  final LoginModel model;
  LoginStateSuccess({
    required this.model,
  });
}

class LoginStateFailed extends LoginStates {
  String msg;
  int errType;
  LoginStateFailed({
    required this.msg,
    required this.errType,
  });
}
