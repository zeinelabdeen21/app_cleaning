import 'package:app_cleaning/controller/nav_bar_bloc/bloc/event.dart';
import 'package:app_cleaning/controller/nav_bar_bloc/bloc/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  // NavigationBloc() : super(NavigationState.initial());
  NavigationBloc() : super(NavigationState.initial()) {
    on<UpdateNavigationIndex>(_getData);
  }

  void _getData(
    NavigationEvent event,
    Emitter<NavigationState> emit,
  ) async {
    if (event is UpdateNavigationIndex) {
      emit(NavigationState(currentIndex: event.newIndex));
    }
  }

  // @override
  // Stream<NavigationState> mapEventToState(NavigationEvent event) async* {
  //   if (event is UpdateNavigationIndex) {
  //     yield NavigationState(currentIndex: event.newIndex);
  //   }
  // }
}
