import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart' as lang;
import 'package:shared_preferences/shared_preferences.dart';

import 'helpers/theme.dart';
import 'kiwi.dart';
import 'routes/app_routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = MyHttpOverrides();
  await lang.EasyLocalization.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  Prefs = await SharedPreferences.getInstance();
  initKiwi();

  runZonedGuarded<Future<void>>(
    () async {
      runApp(
        lang.EasyLocalization(
          path: 'assets/lang',
          saveLocale: true,
          startLocale: const Locale('ar'),
          fallbackLocale: const Locale('en'),
          supportedLocales: const [Locale('ar'), Locale('en')],
          child: const MyApp(),
        ),
      );
    },
    (error, stack) {
      // crash.recordError(error, stack, "run_zoned_guarded");
    },
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(393, 852),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, state) {
        return MaterialApp(
          title: 'ERP',
          initialRoute: AppRoutes.init.initial,
          routes: AppRoutes.init.appRoutes,
          // home: const MyHomePage(title: 'Flutter Demo Home Page'),
          theme: StylesApp.instance.getLightTheme(context.locale),
          // initialRoute: AppRoutes.init.initial,
          // routes: AppRoutes.init.appRoutes,
          // home: HomeView(),
          // navigatorKey: navigator,
          debugShowCheckedModeBanner: false,
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          builder: (context, child) {
            return FlutterEasyLoading(
              child: MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor:
                      context.locale.languageCode == "ar" ? 0.9.sp : 0.9.sp,
                ),
                child: _Unfocus(child: child!),
              ),
            );
          },
        );
      },
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class _Unfocus extends StatefulWidget {
  const _Unfocus({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  __UnfocusState createState() => __UnfocusState();
}

class __UnfocusState extends State<_Unfocus> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: widget.child,
    );
  }
}

late SharedPreferences Prefs;

final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();
