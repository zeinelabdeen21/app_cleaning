import 'package:app_cleaning/controller/home_bloc/bloc.dart';
import 'package:app_cleaning/controller/login_bloc/bloc.dart';
import 'package:kiwi/kiwi.dart';

import 'controller/nav_bar_bloc/bloc/bloc.dart';

void initKiwi() {
  KiwiContainer container = KiwiContainer();

  container.registerFactory((c) => LoginBloc());
  container.registerFactory((c) => NavigationBloc());
  container.registerFactory((c) => HomeBloc());
}
