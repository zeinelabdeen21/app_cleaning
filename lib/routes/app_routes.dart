import '../views/home/home.dart';
import '../views/splash_view/view.dart';
import 'routes.dart';

import 'package:flutter/material.dart';

class AppRoutes {
  static AppRoutes get init => AppRoutes._internal();
  String initial = NamedRoutes.i.splash;

  AppRoutes._internal();
  Map<String, Widget Function(BuildContext context)> appRoutes = {
    NamedRoutes.i.splash: (context) => const SplashView(),
    NamedRoutes.i.home: (context) => const HomeView(),
  };
}
