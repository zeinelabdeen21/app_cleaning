class NamedRoutes {
  static NamedRoutes get i => NamedRoutes._internal();

  NamedRoutes._internal();
  final splash = "/splash";
  final signIn = "/signIn";
  final home = "/home";
}
