import 'dart:convert';

HomeModel homeModelFromJson(String str) => HomeModel.fromJson(json.decode(str));

String homeModelToJson(HomeModel data) => json.encode(data.toJson());

class HomeModel {
  final List<Datum> data;
  final String message;
  final int status;

  HomeModel({
    required this.data,
    required this.message,
    required this.status,
  });

  factory HomeModel.fromJson(Map<String, dynamic> json) => HomeModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message,
        "status": status,
      };
}

class Datum {
  final int id;
  final String image;
  final String titleAr;
  final String titleEn;
  final List<Subsection> subsection;

  Datum({
    required this.id,
    required this.image,
    required this.titleAr,
    required this.titleEn,
    required this.subsection,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        image: json["image"],
        titleAr: json["title_ar"],
        titleEn: json["title_en"],
        subsection: List<Subsection>.from(
            json["subsection"].map((x) => Subsection.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "title_ar": titleAr,
        "title_en": titleEn,
        "subsection": List<dynamic>.from(subsection.map((x) => x.toJson())),
      };
}

class Subsection {
  final int id;
  final String titleAr;
  final String titleEn;
  final int sectionId;

  Subsection({
    required this.id,
    required this.titleAr,
    required this.titleEn,
    required this.sectionId,
  });

  factory Subsection.fromJson(Map<String, dynamic> json) => Subsection(
        id: json["id"],
        titleAr: json["title_ar"],
        titleEn: json["title_en"],
        sectionId: json["section_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title_ar": titleAr,
        "title_en": titleEn,
        "section_id": sectionId,
      };
}
