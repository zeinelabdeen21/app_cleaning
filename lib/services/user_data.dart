import 'package:app_cleaning/views/auth/sign_in/view.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import '../models/model.dart';
import 'dart:convert';

class UserHelper {
  static String accessToken = "";
  static bool get isAuth => accessToken != "";
  static LoginModel userDatum = LoginModel.fromJson({});

  static setUserData(LoginModel data) {
    userDatum = data;
    accessToken = data.data.token;
    Prefs.setString("user_data", json.encode(data.toJson()));
  }

  static Future<bool> getUserData() async {
    String _data = Prefs.getString("user_data") ?? "";
    if (_data != "") {
      userDatum = LoginModel.fromJson(json.decode(_data));
      accessToken = userDatum.data.token;
      return true;
    } else {
      userDatum = LoginModel.fromJson({});
      accessToken = "";
      return false;
    }
  }

  static logout() {
    accessToken = "";
    userDatum = LoginModel.fromJson({});
    Prefs.remove("user_data");
    Navigator.pushAndRemoveUntil(
      navigator.currentContext!,
      MaterialPageRoute(
        builder: (context) => const SignInView(),
      ),
      (Route<dynamic> route) => false,
    );
  }
}

final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();
