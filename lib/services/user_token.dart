import '../main.dart';

Future<Map<String, dynamic>> headersMap() async {
  // String? _lang = preferences.getString("lang");
  Map<String, dynamic> headersData = {
    'Authorization': Prefs.getString("token"),
    // // "Accept-Language": _lang,
    // "Accept": "application/json"
    "Accept": "application/json",
    "token": "${Prefs.getString('token')}",
    "lang": Prefs.getString('lang'),
  };
  return headersData;
}

Future<Map<String, dynamic>> headersMapWithoutToken() async {
  // String? _lang = preferences.getString("lang");

  Map<String, dynamic> headersData = {
    // "Accept-Language": _lang,
    "Accept": "application/json"
  };
  return headersData;
}
