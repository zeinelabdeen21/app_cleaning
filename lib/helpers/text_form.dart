import 'package:app_cleaning/helpers/theme.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class CustomTextField extends StatelessWidget {
  final dynamic validator;
  final dynamic onSaved;
  final String? hintText;
  final bool? enabled;
  final dynamic maxLines;
  final TextInputType? textInputType;
  // final String? prefix;
  final bool? obscureText;
  final Color? color;
  final TextEditingController? controller;
  const CustomTextField({
    super.key,
    this.validator,
    this.onSaved,
    this.hintText,
    this.enabled,
    this.maxLines,
    this.textInputType,
    this.obscureText,
    this.color,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 15,
        left: 15,
      ),
      child: TextFormField(
        validator: validator,
        controller: controller,
        onSaved: onSaved,
        enabled: enabled,
        maxLines: maxLines,
        keyboardType: textInputType,
        obscureText: obscureText ?? false,
        style: const TextStyle(
          //
          color: Color(0xFFC1C1C1),
          fontWeight: FontWeight.bold,
          fontSize: 13,
        ),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.textFormBorderColor,
            ),
          ),
          errorStyle: const TextStyle(
            //
            color: Color(0xFFC1C1C1),
            fontSize: 13,
          ),
          contentPadding:
              const EdgeInsets.only(left: 15, top: 15, bottom: 15, right: 15),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.highlightShimmerColor,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.highlightShimmerColor,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.highlightShimmerColor,
            ),
          ),
          filled: true,
          fillColor: color ?? const Color(0xFFF6F6F6),
          enabled: true,
          labelText: hintText == null ? "" : hintText!.tr(),
          labelStyle: const TextStyle(
            color: Colors.black,
            fontSize: 12,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}

// ********* test bass
class CustomTextFormBass extends StatelessWidget {
  final BuildContext? context;
  final dynamic validator;
  final dynamic onSaved;
  final String? hintText;
  final bool? enabled;
  final Widget? suffix;
  final TextInputType? textInputType;
  // String? prefix,
  final bool? obscureText;
  final TextEditingController? controller;
  const CustomTextFormBass({
    super.key,
    this.context,
    this.validator,
    this.onSaved,
    this.hintText,
    this.enabled,
    this.suffix,
    this.textInputType,
    this.obscureText,
    this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 15,
        left: 15,
      ),
      child: TextFormField(
        validator: validator,
        controller: controller,
        onSaved: onSaved,
        enabled: enabled,
        //   textAlign: TextAlign.center,
        keyboardType: textInputType,
        obscureText: obscureText ?? false,

        style: const TextStyle(
          color: Color(0xFFC1C1C1),
          fontWeight: FontWeight.bold,
          fontSize: 13,
        ),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.textFormBorderColor,
            ),
          ),
          errorStyle: const TextStyle(
            color: Color(0xFFC1C1C1),
            fontSize: 13,
          ),
          // prefixIcon: Padding(
          //   padding: const EdgeInsets.all(12.0),
          //   child: Image.asset(
          //     prefix ?? "",
          //     width: 20,
          //     height: 20,
          //   ),
          // ),
          contentPadding:
              const EdgeInsets.only(left: 15, top: 15, bottom: 15, right: 15),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.textFormBorderColor,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.highlightShimmerColor,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(25),
            ),
            borderSide: BorderSide(
              width: 1,
              style: BorderStyle.solid,
              color: StylesApp.instance.highlightShimmerColor,
            ),
          ),
          filled: true,
          fillColor: Color(0xFFF6F6F6),
          enabled: true,
          labelText: hintText == null ? "" : hintText!.tr(),
          labelStyle: const TextStyle(
            color: Colors.black,
            fontSize: 12,
            fontWeight: FontWeight.w600,
          ),
          suffix: suffix,
        ),
      ),
    );
  }
}

class TxtField extends StatelessWidget {
  final dynamic validator;
  final dynamic onSaved;
  final String hintText;
  final bool enabled;
  final TextInputType textInputType;
  final Widget prefix;
  final bool obscureText;
  final TextEditingController controller;
  const TxtField(
      {Key? key,
      this.validator,
      this.onSaved,
      required this.hintText,
      required this.enabled,
      required this.textInputType,
      required this.prefix,
      required this.obscureText,
      required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 15,
        left: 15,
      ),
      child: TextFormField(
        validator: validator,
        controller: controller,
        onSaved: onSaved,
        enabled: enabled,
        //   textAlign: TextAlign.center,
        keyboardType: textInputType,
        obscureText: obscureText,

        style: const TextStyle(
          // fontFamily: AppTheme.boldFont,
          color: Color(0xFFC1C1C1),
          fontWeight: FontWeight.bold,
          fontSize: 13,
        ),
        decoration: InputDecoration(
          enabledBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              width: 0.5,
              style: BorderStyle.solid,
              color: Colors.black,
            ),
          ),
          errorStyle: const TextStyle(
            // fontFamily: AppTheme.boldFont,
            color: Color(0xFFC1C1C1),
            fontSize: 13,
          ),
          prefixIcon: Padding(
            padding: const EdgeInsets.all(12.0),
            child: prefix,
          ),
          contentPadding:
              const EdgeInsets.only(left: 15, top: 20, bottom: 20, right: 15),
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              width: 0.5,
              style: BorderStyle.solid,
              color: Colors.black,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              width: 0.5,
              style: BorderStyle.solid,
              color: Colors.black,
            ),
          ),
          focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              width: 0.5,
              style: BorderStyle.solid,
              color: Colors.black,
            ),
          ),
          filled: true,
          fillColor: Colors.white,
          enabled: true,
          labelText: hintText == null ? "" : hintText.tr(),
          labelStyle: const TextStyle(
            color: Color(0xFFC1C1C1),
            fontSize: 12,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
